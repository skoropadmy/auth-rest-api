FROM node

WORKDIR /root/app

COPY package.json /root/app

RUN npm install && npm install gulp -g

COPY . /root/app

RUN gulp dist && rm -rf temp node_modules app
