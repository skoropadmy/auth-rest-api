# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]
#### Added
- Add docker-compose.yml and Dockerfile.local files

### Changed
- Rename app from harno-auth to harno-auth-rest-api, update package.json
- Update dependencies version
- Rename some functions
- Update username validation

### Fixed
- Fix .gitlab-ci.yml

## [0.0.1] - 2018-12-29
### Added
- Add app.js file that configures whole app
- Add Authorization service that is used for creating middlewares
- Add Authorization router that manages routes
- Add Profile model service that creates and manages mongoose model
- Add tests for services
