# Harno-auth
App that can be used to implement authorization

## Starting an app
To start app you need to have mongodb running


## Starting app with docker-compose
If you want to start app with dependent services starting automatically you need to have installed docker-compose.Then you need to run
```console
docker-compose up --build
```
This will download, build and start all needed services and also build and start 'mugir-web-server' container from local Dockerfile. To stop all services you can press ctrl+c and it will stop (not remove) every container.

To stop and destroy containers you need to run
```console
docker-compose down
```

The app itself will be running on port 3002
