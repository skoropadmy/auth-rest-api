'use strict'
/* global describe, it, afterEach, after, before */
import should from 'should'
import sinon from 'sinon'
import App from '../app'

const sandbox = sinon.createSandbox()

const app = new App()

let profilesModel

const testProfile = {
  username: 'some_username',
  email: 'some_email',
  password: 'some_password'
}
/**
 * @test {ProfilesModel}
 */

describe('Profile model service ', () => {
  before(async () => {
    await app.start()
    profilesModel = app.kernel.s.profilesModel
  })
  afterEach(async () => {
    await profilesModel.Profiles.deleteMany()
    sandbox.restore()
  })
  after(async () => {
    await app.stop()
  })

  /**
   * @test {ProfilesModel#getProfile}
   */
  it('should find profile', async () => {
    await profilesModel.Profiles({ ...testProfile, id: 'thisIsId' }).save()

    const actualProfile = await profilesModel.getProfile({ username: testProfile.username })
    actualProfile.should.containDeep({ ...testProfile, id: 'thisIsId' })
  })

  /**
   * @test {ProfilesModel#createProfile}
   */
  it('should create profile', async () => {
    await profilesModel.createProfile(testProfile)

    const actualProfile = await profilesModel.getProfile({ username: testProfile.username })
    actualProfile.should.containDeep(testProfile)
    actualProfile.should.have.property('id')
  })

  /**
   * @test {ProfilesModel#deleteProfile}
   */
  it('should delete profile', async () => {
    await profilesModel.createProfile(testProfile)

    await profilesModel.deleteProfile({ username: testProfile.username })
    const actualProfile = await profilesModel.getProfile({ username: testProfile.username })
    should.not.exist(actualProfile)
  })

  /**
   * @test {ProfilesModel#updatePassword}
   */
  it('should update profile password', async () => {
    const profile = await profilesModel.createProfile(testProfile)
    await profile.updatePassword('new pass')
    const actualProfile = await profilesModel.getProfile({ username: testProfile.username })
    actualProfile.password.should.be.exactly('new pass')
  })
})
