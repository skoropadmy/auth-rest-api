'use strict'

import uuid from 'uuid/v4'
/**
 * Registers profiles model for mongoDB and functions to work with this model
 */
export default class ProfilesModel {
  /**
   * Registers profiles model for mongoDB
   */
  registerProfilesModel () {
    const mongoose = this._d.mongoDB.mongoose
    const Schema = mongoose.Schema
    const fields = {
      id: {
        type: String,
        required: true,
        unique: true
      },
      username: {
        type: String,
        required: true,
        unique: true
      },
      email: {
        type: String,
        required: true,
        unique: true
      },
      password: {
        type: String,
        required: true
      },
      profileToken: {
        type: String
      }
    }

    const profilesSchema = new Schema(fields)

    profilesSchema.methods.updatePassword = this.updatePassword

    delete mongoose.connection.models.Profiles

    this._Profiles = mongoose.model('Profiles', profilesSchema)
  }

  /**
   * Creates a profile
   * @param {object} profile object with id, username, password and email properties
   * @return {Promise<Object>} resolves with a created profile
   */
  createProfile (profile) {
    profile.id = uuid()
    return this._Profiles(profile).save()
  }

  /**
   * Creates a profile token
   * @param {object} profile object with id, username, password and email properties
   * @return {Promise<Object>} resolves with a token
   */
  createToken (profile) {
    profile.profileToken = uuid()
    return this._Profiles(profile).save()
  }

  /**
   * Gets a profile
   * @param {object} profile object with id, username, password and email properties
   * @return {Promise<Object>} resolves with a profile
   */
  getProfile (profile) {
    return this._Profiles.findOne(profile)
  }

  /**
   * Deletes profile
   * @param {object} profile object with id, username, password and email properties
   * @return {Promise<Object>} resolves with a deleted profile
   */
  deleteProfile (profile) {
    return this._Profiles.findOneAndDelete(profile)
  }

  /**
   * Creates a profile token
   * @param {string} password string that will become password of profile
   * @return {Promise<Object>} resolves with an updated profile
   */
  updatePassword (password) {
    this.password = password
    return this.save()
  }

  /**
   * Gets mongoose model
   * @return {Promise<Object>} resolves with a mongoose model
   */
  get Profiles () {
    return this._Profiles
  }
}
