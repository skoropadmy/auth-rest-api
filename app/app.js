import ServerKernel from 'harno-kernel/dist/wrappers/server/server.kernel'

import ProfilesModel from './profiles/profiles.model'
import AuthorizationService from './authorization/authorization.service'
import AuthorizationRouter from './authorization/authorization.router'

import path from 'path'
import fs from 'fs'

/**
 * Class that register and start services and register routes
 */
export default class App {
  /**
   * Inits kernel module
   */
  constructor () {
    this.kernel = new ServerKernel()
    this.s = this.kernel.s

    const appInfo = require(path.resolve(__dirname, '../package.json'))
    this._version = appInfo.version
    this._description = appInfo.description
  }

  /**
   * Registers services, routes, registers models and starts the app
   * @return {Promise} resolves when app is ready
   */
  async start () {
    this._registerServices()
    this._registerDependencies()
    this._initServices()
    this._registerModels()
    this._regiserRoutes()
    await this._startServer()
  }

  /**
   * Registers services
   */
  _registerServices () {
    this.kernel.registerDefaultServices()

    this.kernel.registerService('profilesModel', ProfilesModel)
    this.kernel.registerService('authorizationService', AuthorizationService)
    this.kernel.registerService('authorizationRouter', AuthorizationRouter)
  }

  /**
   * Register dependencies
   */
  _registerDependencies () {
    this.kernel.initDefaultServicesDependencies()

    this.kernel.initDependencies({
      profilesModel: ['mongoDB'],
      authorizationService: ['config', 'profilesModel', 'errorHandler', 'validation', 'logger'],
      authorizationRouter: ['config', 'express', 'authorizationService', 'errorHandler'] })
  }

  /**
   * Inits services
   */
  _initServices () {
    this.s.config.setConfigYML(fs.readFileSync(path.resolve(__dirname, './config.yml')))
    this.s.config.setDefaultsYML(fs.readFileSync(path.resolve(__dirname, './config.defaults.yml')))
    this.c = this.s.config.c

    this.s.logger.init(this.c.logger)

    this.kernel.s.authorizationService.init()

    this.kernel.initDefaultServices()
  }

  /**
   * Registers models
   */
  _registerModels () {
    this.kernel.s.profilesModel.registerProfilesModel()
  }

  /**
   * Registers routes
   */
  _regiserRoutes () {
    const expressService = this.kernel.s.express

    expressService.registerRoute('/', expressService.bodyParser.json())

    expressService.registerRoute('/', this.kernel.s.authorizationRouter.registerAuthorizationRoutes())

    expressService.registerSwaggerRoute('/swagger.json', {
      title: 'REST API',
      description: this._description,
      license: {
        name: 'Copyrights Harno Soft, LLC, 2019. All rights reserved.'
      },
      version: this._version
    })

    expressService.registerRoute('/', this.kernel.s.errorHandler.expressErrorHandler)
  }

  /**
   * Connects to mongoDB, establishes express connection
   * @return {Promise} resolves when express connection is established
   */
  async _startServer () {
    await this.kernel.s.mongoDB.tryToConnect(this.c.db.url)
    await this.kernel.s.express.start(this.c.port)
  }

  /**
   * Stops express and mongodb connections
   * @return {Promise} resolves when express connection is closed
   */
  async stop () {
    await this.kernel.s.mongoDB.disconnect()
    await this.kernel.s.express.close()
  }
}
