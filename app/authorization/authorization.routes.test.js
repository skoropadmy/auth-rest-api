/* global describe, it, before, after, afterEach */
import App from '../app.js'
import 'should'
import request from 'supertest-promised'
import sinon from 'sinon'

const app = new App()
let profilesModel, secretToken

const testProfile = {
  username: 'profileName',
  email: 'mameil@uamail.com',
  password: 'someStrong42_password',
  id: 'some_id'
}

/**
 * @test {AuthorizationService}
 */
describe('Authorization Service', () => {
  before(async () => {
    await app.start()
    profilesModel = app.kernel.s.profilesModel
    secretToken = app.kernel.s.config.c.authorization.secretToken
    await profilesModel.Profiles.deleteMany()
  })

  afterEach(async () => {
    await profilesModel.Profiles.deleteMany()
  })

  after(async () => {
    await app.stop()
  })

  /**
   * @test {AuthorizationService#createProfile}
   * @test {AuthorizationService#deleteProfile}
   * @test {AuthorizationService#getProfile}
   * @test {AuthorizationService#updatePassword}
   * @test {AuthorizationService#createProfileToken}
   * @test {AuthorizationService#validateProfileToken}
   */
  it('should respond with UnauthorizedError when secret token in headers is invalid', async () => {
    const response1 = await request(app.kernel.s.express.app)
      .post('/profile/profileName')
      .set('Secret-Token', 'notToken')
      .send()
      .expect(401)
      .end()
      .get('body')

    response1.should.be.deepEqual({
      error: 'UnauthorizedError',
      message: 'Secret Token is invalid'
    })

    const response2 = await request(app.kernel.s.express.app)
      .get('/profile/profileName')
      .set('Secret-Token', 'notToken')
      .send()
      .expect(401)
      .end()
      .get('body')

    response2.should.be.deepEqual({
      error: 'UnauthorizedError',
      message: 'Secret Token is invalid'
    })

    const response3 = await request(app.kernel.s.express.app)
      .put('/profile/profileName')
      .set('Secret-Token', 'notToken')
      .send()
      .expect(401)
      .end()
      .get('body')

    response3.should.be.deepEqual({
      error: 'UnauthorizedError',
      message: 'Secret Token is invalid'
    })

    const response4 = await request(app.kernel.s.express.app)
      .delete('/profile/profileName')
      .set('Secret-Token', 'notToken')
      .send()
      .expect(401)
      .end()
      .get('body')

    response4.should.be.deepEqual({
      error: 'UnauthorizedError',
      message: 'Secret Token is invalid'
    })

    const response5 = await request(app.kernel.s.express.app)
      .post('/profile/profileName/token')
      .set('Secret-Token', 'notToken')
      .send()
      .expect(401)
      .end()
      .get('body')

    response5.should.be.deepEqual({
      error: 'UnauthorizedError',
      message: 'Secret Token is invalid'
    })

    const response6 = await request(app.kernel.s.express.app)
      .post('/profile/profileName/validate-token')
      .set('Secret-Token', 'notToken')
      .send()
      .expect(401)
      .end()
      .get('body')

    response6.should.be.deepEqual({
      error: 'UnauthorizedError',
      message: 'Secret Token is invalid'
    })

    const actualProfiles = await profilesModel.Profiles.find({})
    actualProfiles.should.be.deepEqual([])
  })

  /**
   * @test {AuthorizationService#createProfile}
   */
  it('should create profile', async () => {
    await request(app.kernel.s.express.app)
      .post('/profile/profileName1')
      .set('Secret-Token', secretToken)
      .send({ email: 'mameil@uamail.com', password: 'someStrong42_password' })
      .expect(204)
      .end()
    await request(app.kernel.s.express.app)
      .post('/profile/profileName2')
      .set('Secret-Token', secretToken)
      .send({ email: 'mameil@uamail.com2', password: 'someStrong42_password' })
      .expect(204)
      .end()

    const actualProfile1 = await profilesModel.Profiles.findOne({ username: 'profileName1' })
    const actualProfile2 = await profilesModel.Profiles.findOne({ username: 'profileName2' })

    actualProfile1.should.containDeep({
      username: 'profileName1',
      password: 'someStrong42_password',
      email: 'mameil@uamail.com'
    })
    actualProfile2.should.containDeep({
      username: 'profileName2',
      password: 'someStrong42_password',
      email: 'mameil@uamail.com2'
    })
  })

  /**
   * @test {AuthorizationService#createProfile}
   */
  it('should respond with error if password is not strong password or if email is not email', async () => {
    await request(app.kernel.s.express.app)
      .post('/profile/profileName')
      .set('Secret-Token', secretToken)
      .send({ email: 'mameil@uamail.com', password: 'not_strong' })
      .expect(400)
      .end()

    await request(app.kernel.s.express.app)
      .post('/profile/profileName')
      .set('Secret-Token', secretToken)
      .send({ email: 'not_email', password: 'someStrong42_password2' })
      .expect(400)
      .end()

    const actualProfiles = await profilesModel.Profiles.find({})
    actualProfiles.length.should.be.exactly(0)
  })

  /**
   * @test {AuthorizationService#createProfile}
   */
  it('should respond with an error code if profile already exists', async () => {
    await profilesModel.createProfile(testProfile)

    await request(app.kernel.s.express.app)
      .post('/profile/profileName2')
      .set('Secret-Token', secretToken)
      .send({ email: 'mameil@uamail.com', password: 'someStrong42_password2' })
      .expect(400)
      .end()

    await request(app.kernel.s.express.app)
      .post('/profile/profileName')
      .set('Secret-Token', secretToken)
      .send({ email: 'mameil@uamail.com3', password: 'someStrong42_password3' })
      .expect(400)
      .end()

    let stubbedCreate

    try {
      const sandbox = sinon.createSandbox()
      stubbedCreate = sandbox.stub(profilesModel, 'createProfile').throws({ code: 11000, message: 'This error is not connected to email or username' })

      await request(app.kernel.s.express.app)
        .post('/profile/profileName')
        .set('Secret-Token', secretToken)
        .send({ email: 'mameil@uamail.com', password: 'someStrong42_password' })
        .expect(400)
        .end()
    } catch (e) {
      stubbedCreate.restore()
      throw e
    }
    stubbedCreate.restore()

    const actualProfiles = await profilesModel.Profiles.find({})
    actualProfiles.length.should.be.exactly(1)
    actualProfiles[0].should.containDeep(testProfile)
  })

  /**
   * @test {AuthorizationService#createProfile}
   */
  it('should respond with an error code if there was unexpected problems in db', async () => {
    const sandbox = sinon.createSandbox()
    const stubbedCreate = sandbox.stub(profilesModel, 'createProfile').throws(new Error('Some err'))

    await request(app.kernel.s.express.app)
      .post('/profile/profileName')
      .set('Secret-Token', secretToken)
      .send({ email: 'mameil@uamail.com', password: 'someStrong42_password' })
      .expect(400)
      .end()

    stubbedCreate.restore()
  })

  /**
   * @test {AuthorizationService#getProfile}
   */
  it('should respond with a profile', async () => {
    await profilesModel.createProfile(testProfile)

    const profile = await request(app.kernel.s.express.app)
      .get('/profile/profileName')
      .set('Secret-Token', secretToken)
      .expect(200)
      .end()
      .get('body')

    const actualProfile = await profilesModel.Profiles.findOne({ username: 'profileName' })
    profile.should.containDeep({
      username: 'profileName',
      password: 'someStrong42_password',
      email: 'mameil@uamail.com'
    })
    profile.id.should.be.exactly(actualProfile.id)
  })

  /**
   * @test {AuthorizationService#getProfile}
   */
  it('should respond with an error code if profile was not found', async () => {
    await request(app.kernel.s.express.app)
      .get('/profile/profileName')
      .set('Secret-Token', secretToken)
      .expect(404)
      .end()
  })

  /**
   * @test {AuthorizationService#deleteProfile}
   */
  it('should delete profile', async () => {
    await profilesModel.createProfile(testProfile)

    await request(app.kernel.s.express.app)
      .delete('/profile/profileName')
      .set('Secret-Token', secretToken)
      .expect(204)
      .end()

    const actualProfiles = await profilesModel.Profiles.find({})
    actualProfiles.should.be.deepEqual([])
  })

  /**
   * @test {AuthorizationService#UpdatePassword}
   */
  it('should update password', async () => {
    await profilesModel.createProfile(testProfile)

    await request(app.kernel.s.express.app)
      .put('/profile/profileName')
      .set('Secret-Token', secretToken)
      .send({ password: 'someStrong42_password2' })
      .expect(204)
      .end()

    const actualProfile = await profilesModel.Profiles.findOne({ username: 'profileName' })

    actualProfile.should.containDeep({ password: 'someStrong42_password2' })
  })

  /**
  * @test {AuthorizationService#updatePassword}
  */
  it('should respond with an error code if profile was not found', async () => {
    await request(app.kernel.s.express.app)
      .put('/profile/profileName')
      .set('Secret-Token', secretToken)
      .send({ password: testProfile.password })
      .expect(404)
      .end()
  })

  /**
   * @test {AuthorizationService#createProfileToken}
   */
  it('should add token to profile and respond with this token', async () => {
    await profilesModel.createProfile(testProfile)

    const actualProfile1 = profilesModel.Profiles.find({ username: 'username' })
    actualProfile1.should.not.have.property('profileToken')

    const profile = await request(app.kernel.s.express.app)
      .post('/profile/profileName/token')
      .set('Secret-Token', secretToken)
      .send({ password: 'someStrong42_password' })
      .expect(200)
      .end()
      .get('body')
    const actualProfile2 = await profilesModel.Profiles.findOne({ username: 'profileName' })

    profile.should.have.property('profileToken')
    profile.profileToken.should.be.exactly(actualProfile2.profileToken)
  })

  /**
  * @test {AuthorizationService#createProfileToken}
  */
  it('should respond with an error code if password is invalid', async () => {
    await profilesModel.createProfile(testProfile)

    await request(app.kernel.s.express.app)
      .post('/profile/profileName/token')
      .set('Secret-Token', secretToken)
      .send({ password: 'not_Real1_pass_but_strong' })
      .expect(401)
      .end()

    const actualProfile1 = profilesModel.Profiles.find({ username: 'username' })
    actualProfile1.should.not.have.property('profileToken')
  })

  /**
  * @test {AuthorizationService#createProfileToken}
  */
  it('should respond with an error code if profile was not found', async () => {
    await request(app.kernel.s.express.app)
      .post('/profile/profileName/token')
      .set('Secret-Token', secretToken)
      .send({ password: testProfile.password })
      .expect(404)
      .end()
  })

  /**
  * @test {AuthorizationService#createProfileToken}
  */
  it('should respond with an error code if profile token was not passed in headers', async () => {
    await request(app.kernel.s.express.app)
      .post('/profile/profileName/token')
      .set('Secret-Token', secretToken)
      .send()
      .expect(400)
      .end()
  })

  /**
  * @test {AuthorizationService#validateProfileToken}
  */
  it('should validate profile token', async () => {
    const profile = await profilesModel.createProfile(testProfile)
    const { profileToken } = await profilesModel.createToken(profile)
    await request(app.kernel.s.express.app)
      .post('/profile/profileName/validate-token')
      .set({ 'Secret-Token': secretToken, 'Profile-Token': profileToken })
      .send({ profileToken })
      .expect(204)
      .end()
  })

  /**
   * @test {AuthorizationService#validateProfileToken}
   */
  it('should respond with an error code if profile was not found', async () => {
    await request(app.kernel.s.express.app)
      .post('/profile/profileName/validate-token')
      .set({ 'Secret-Token': secretToken, 'Profile-Token': 'notReal' })
      .send({ token: 'profileToken' })
      .expect(404)
      .end()
  })

  /**
   * @test {AuthorizationService#validateProfileToken}
   */
  it('should respond with error code if profile token is invalid', async () => {
    const profile = await profilesModel.createProfile(testProfile)
    await profilesModel.createToken(profile)

    await request(app.kernel.s.express.app)
      .post('/profile/profileName/validate-token')
      .set({ 'Secret-Token': secretToken, 'Profile-Token': 'notReal' })
      .send({ profileToken: 'notRealToken' })
      .expect(401)
      .end()
  })
})
