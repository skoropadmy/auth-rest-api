'use strict'
/**
 * Class that handles requests to authorization service
 */
export default class AuthorizationAPIService {
  /**
   * Creates logger
   */
  init () {
    this._logger = this._d.logger.createLogger('AuthorizationService')
  }

  /**
   * Returns middleware that creates and adds profile
   * @return {Function(req, res, next)} creates and adds profile
   */
  get createProfile () {
    return this._createProfile.bind(this)
  }

  /**
   * Returns middleware that returns profile
   * @return {Function(req, res, next)} returns profile
   */
  get getProfile () {
    return this._getProfile.bind(this)
  }

  /**
   * Returns middleware that deletes profile
   * @return {Function(req, res, next)} deletes profile
   */
  get deleteProfile () {
    return this._deleteProfile.bind(this)
  }

  /**
   * Returns middleware that updates password of profile
   * @return {Function(req, res, next)} updates password of profile
   */
  get updatePassword () {
    return this._updatePassword.bind(this)
  }

  /**
   * Returns middleware that creates and adds token to profile
   * @return {Function(req, res, next)} adds token to profile
   */
  get createProfileToken () {
    return this._createProfileToken.bind(this)
  }

  /**
   * Returns middleware that validates profile token
   * @return {Function(req, res, next)} validates profile token
   */
  get validateProfileToken () {
    return this._validateProfileToken.bind(this)
  }

  /**
   * Creates a profile with username, password and email
   * @param {Object} req express request object
   * @param {Object} res express responce object
   * @param {Function} next sends error to next middleware
   */
  async _createProfile (req, res, next) {
    try {
      this._validateSecretToken(req.get('Secret-Token'))

      this._validateUsername(req.params.username)

      this._validateBodyWithPasswordAndEmail(req.body)

      try {
        await this._d.profilesModel.createProfile({
          username: req.params.username,
          password: req.body.password,
          email: req.body.email
        })
      } catch (e) {
        if (e.code === 11000) {
          if (e.message.match(/email_1 dup key/)) {
            e.message = `This email ${e.message.match(/".+"/)} is already used by another user`
          } else if (e.message.match(/username_1 dup key/)) {
            e.message = `This username ${e.message.match(/".+"/)} is already used by another user`
          } else {
            e.message = `One of unique values are used by another user`
          }
        }
        throw new this._d.errorHandler.errors.ValidationError(e)
      }
      this._logger.info('Profile was successfully created')
      res.sendStatus(204)
    } catch (e) {
      next(e)
    }
  }

  /**
   * Gets a profile by username
   * @param {Object} req express request object
   * @param {Object} res express responce object
   * @param {Function} next sends error to next middleware
   */
  async _getProfile (req, res, next) {
    try {
      this._validateSecretToken(req.get('Secret-Token'))

      this._validateUsername(req.params.username)

      const profile = await this._d.profilesModel.getProfile({ username: req.params.username })
      if (!profile) {
        throw new this._d.errorHandler.errors.NotFoundError('Profile was not found')
      }
      this._logger.info('Profile was successfully returned')
      res.json(profile)
    } catch (e) {
      next(e)
    }
  }

  /**
   * Deletes a profile by username
   * @param {Object} req express request object
   * @param {Object} res express responce object
   * @param {Function} next sends error to next middleware
   */
  async _deleteProfile (req, res, next) {
    try {
      this._validateSecretToken(req.get('Secret-Token'))

      this._validateUsername(req.params.username)

      await this._d.profilesModel.deleteProfile({ username: req.params.username })
      this._logger.info('Profile was successfully deleted')
      res.sendStatus(204)
    } catch (e) {
      next(e)
    }
  }

  /**
   * Updates password by username
   * @param {Object} req express request object
   * @param {Object} res express responce object
   * @param {Function} next sends error to next middleware
   */
  async _updatePassword (req, res, next) {
    try {
      this._validateSecretToken(req.get('Secret-Token'))

      this._validateUsername(req.params.username)

      this._validateBodyWithPassword(req.body)

      const profile = await this._d.profilesModel.getProfile({ username: req.params.username })
      if (!profile) {
        throw new this._d.errorHandler.errors.NotFoundError('Profile was not found')
      }

      await profile.updatePassword(req.body.password)

      this._logger.info('Profile password was successfully updated')
      res.sendStatus(204)
    } catch (e) {
      next(e)
    }
  }

  /**
   * Creates a profile token by username
   * @param {Object} req express request object
   * @param {Object} res express responce object
   * @param {Function} next sends error to next middleware
   */
  async _createProfileToken (req, res, next) {
    try {
      this._validateSecretToken(req.get('Secret-Token'))

      this._validateUsername(req.params.username)

      this._validateBodyWithPassword(req.body)

      const profile = await this._d.profilesModel.getProfile({ username: req.params.username })
      if (!profile) {
        throw new this._d.errorHandler.errors.NotFoundError('Profile was not found')
      } else if (req.body.password !== profile.password) {
        throw new this._d.errorHandler.errors.UnauthorizedError('Wrong password')
      }

      const { profileToken } = await this._d.profilesModel.createToken(profile)
      this._logger.info('Profile token successfully created')
      res.json({ profileToken })
    } catch (e) {
      next(e)
    }
  }

  /**
   * Checks if token from request is same as token in DB
   * @param {Object} req express request object
   * @param {Object} res express responce object
   * @param {Function} next sends error to next middleware
   */
  async _validateProfileToken (req, res, next) {
    try {
      this._validateSecretToken(req.get('Secret-Token'))

      this._validateUsername(req.params.username)

      this._d.validation.isString().validate(req.get('Profile-Token'), 'profileToken')

      const profile = await this._d.profilesModel.getProfile({ username: req.params.username })
      if (!profile) {
        throw new this._d.errorHandler.errors.NotFoundError('Profile was not found')
      } else if (!profile.profileToken || profile.profileToken !== req.get('Profile-Token')) {
        throw new this._d.errorHandler.errors.UnauthorizedError('Profile Token is invalid')
      }
      this._logger.info('Token passed validation')
      res.sendStatus(204)
    } catch (e) {
      next(e)
    }
  }

  /**
   * Checks if username is valid
   * @param {string} username username that should be checked
   * @throws {Error} if username is not valid
   * @throws {Error} if username is contains not only a-z, A-Z, _, - symbols
   */
  _validateUsername (username) {
    this._d.validation.isString({ regexp: /^[A-Za-z0-9_-]+$/ }).validate(username, 'username')
  }

  /**
   * Checks if token from header is same as token in config file
   * @param {string} secretToken token that should be checked
   * @throws {Error} if secret token from header is not valid
   */
  _validateSecretToken (secretToken) {
    this._d.validation.isString().validate(secretToken, 'secterToken')

    const valid = secretToken === this._d.config.c.authorization.secretToken
    if (!valid) {
      throw new this._d.errorHandler.errors.UnauthorizedError('Secret Token is invalid')
    }
  }

  /**
   * Checks if token from header is same as token in config file
   * @param {Object} body body object from request
   * @throws {Error} if body.password is not valid strong password
   * @throws {Error} if body.email is not valid email
   */
  _validateBodyWithPasswordAndEmail (body) {
    const validator = this._d.validation.isObject({
      required: {
        password: this._d.validation.isStrongPassword(),
        email: this._d.validation.isEmail()
      }
    })
    validator.validate(body, 'body')
  }

  /**
   * Checks if token from header is same as token in config file
   * @param {Object} body body object from request
   * @throws {Error} if body.password is not valid strong password
   * @throws {Error} if body.email is not valid email
   */
  _validateBodyWithPassword (body) {
    const validator = this._d.validation.isObject({
      required: {
        password: this._d.validation.isStrongPassword()
      }
    })
    validator.validate(body, 'body')
  }
}
