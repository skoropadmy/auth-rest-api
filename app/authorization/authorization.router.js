'use strict'
/**
 * Class that registeres routes
 */
export default class RouterService {
/**
 * Registeres routes
 */
  registerAuthorizationRoutes () {
    this._d.express.addSwaggerSchema('ProfileToken', {
      type: 'object',
      required: ['profileToken'],
      properties: {
        profileToken: {
          description: 'Profile token',
          type: 'string'
        }
      }
    })
    this._d.express.addSwaggerSchema('ProfilePasswordAndEmail', {
      type: 'object',
      required: ['email', 'password'],
      properties: {
        email: {
          description: 'Profile email',
          type: 'string'
        },
        password: {
          description: 'Profile password',
          type: 'string'
        }
      }
    })
    this._d.express.addSwaggerSchema('ProfilePassword', {
      type: 'object',
      required: ['password'],
      properties: {
        password: {
          description: 'Profile password',
          type: 'string'
        }
      }
    })
    this._d.express.addSwaggerSchema('Profile', {
      type: 'object',
      required: ['email', 'password', 'username'],
      properties: {
        email: {
          description: 'Profile email',
          type: 'string'
        },
        password: {
          description: 'Profile password',
          type: 'string'
        },
        username: {
          description: 'Profile username',
          type: 'string'
        }
      }
    })
    const router = this._d.express.createRouter()
    router.route('/profile/:username')
      .spec({
        post: {
          description: 'Creates profile',
          parameters: [{
            name: 'Secret-Token',
            description: 'Token for validation',
            in: 'header',
            type: 'string',
            required: true
          }, {
            name: 'username',
            description: 'Profile name',
            in: 'path',
            type: 'string',
            required: true
          }, {
            name: 'ProfilePasswordAndEmail',
            description: 'Password and email of profile',
            in: 'body',
            schema: {
              $ref: '#/definitions/ProfilePasswordAndEmail'
            },
            required: true
          }],
          responses: {
            '204': {
              description: 'Profile succesfully created and added to db'
            },
            '401': {
              description: 'Secret token is invaild',
              schema: {
                $ref: '#/definitions/Error'
              }
            },
            '400': {
              description: 'Validation problems',
              schema: {
                $ref: '#/definitions/Error'
              }
            }
          }
        },
        get: {
          description: 'Gets the profile from db',
          parameters: [{
            name: 'Secret-Token',
            description: 'Token for validation',
            in: 'header',
            type: 'string',
            required: true
          }, {
            name: 'username',
            description: 'Profile name',
            in: 'path',
            type: 'string',
            required: true
          }],
          responses: {
            '200': {
              description: 'Profile successfully returned',
              schema: {
                $ref: '#/definitions/Profile'
              }
            },
            '401': {
              description: 'Secret token is invaild',
              schema: {
                $ref: '#/definitions/Error'
              }
            },
            '404': {
              description: 'Profile was not found',
              schema: {
                $ref: '#/definitions/Error'
              }
            }
          }
        },
        put: {
          description: 'Updates password of profile',
          parameters: [{
            name: 'Secret-Token',
            description: 'Token for validation',
            in: 'header',
            type: 'string',
            required: true
          }, {
            name: 'username',
            description: 'Profile name',
            in: 'path',
            type: 'string',
            required: true
          }],
          responses: {
            '204': {
              description: 'Updated password'
            },
            '401': {
              description: 'Secret token is invaild',
              schema: {
                $ref: '#/definitions/Error'
              }
            },
            '404': {
              description: 'Profile was not found',
              schema: {
                $ref: '#/definitions/Error'
              }
            }
          }
        },
        delete: {
          description: 'Deletes profile from db',
          parameters: [{
            name: 'Secret-Token',
            description: 'Token for validation',
            in: 'header',
            type: 'string',
            required: true
          }, {
            name: 'username',
            description: 'Profile name',
            in: 'path',
            type: 'string',
            required: true
          }],
          responses: {
            '204': {
              description: 'Deleted profile from db'
            },
            '401': {
              description: 'Secret token is invaild',
              schema: {
                $ref: '#/definitions/Error'
              }
            }
          }
        }
      })
      .post(this._d.authorizationService.createProfile)
      .get(this._d.authorizationService.getProfile)
      .put(this._d.authorizationService.updatePassword)
      .delete(this._d.authorizationService.deleteProfile)

    router.route('/profile/:username/token')
      .spec({
        post: {
          description: 'Creates profile token',
          parameters: [{
            name: 'Secret-Token',
            description: 'Token for validation',
            in: 'header',
            type: 'string',
            required: true
          }, {
            name: 'username',
            description: 'Profile name',
            in: 'path',
            type: 'string',
            required: true
          }, {
            name: 'ProfilePassword',
            description: 'Password of profile',
            in: 'body',
            schema: {
              $ref: '#/definitions/ProfilePassword'
            }
          }],
          responses: {
            '200': {
              description: 'Created and added token to profile',
              schema: {
                $ref: '#/definitions/ProfileToken'
              }
            },
            '401': {
              description: 'Secret token or password is invaild',
              schema: {
                $ref: '#/definitions/Error'
              }
            },
            '404': {
              description: 'Profile was not found',
              schema: {
                $ref: '#/definitions/Error'
              }
            }
          }
        }
      })
      .post(this._d.authorizationService.createProfileToken)

    router.route('/profile/:username/validate-token')
      .spec({
        post: {
          description: 'Validates profile token',
          parameters: [{
            name: 'Secret-Token',
            description: 'Token for validation',
            in: 'header',
            type: 'string',
            required: true
          }, {
            name: 'Profile-Token',
            description: 'Token for profile validation',
            in: 'header',
            type: 'string',
            required: true
          }, {
            name: 'username',
            description: 'Profile name',
            in: 'path',
            type: 'string',
            required: true
          }, {
            name: 'ProfileToken',
            description: 'Profile token',
            in: 'body',
            schema: {
              $ref: '#/definitions/ProfileToken'
            }
          }],
          responses: {
            '204': {
              description: 'Profile token is valid'
            },
            '401': {
              description: 'Secret token is invalid or profile token is invalid or profile token is not created yet',
              schema: {
                $ref: '#/definitions/Error'
              }
            },
            '404': {
              description: 'Profile was not found',
              schema: {
                $ref: '#/definitions/Error'
              }
            }
          }
        }
      })
      .post(this._d.authorizationService.validateProfileToken)

    return router
  }
}
